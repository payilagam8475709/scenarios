abstract class SmartPhone {
	
	// Abstract Methods
	abstract int call (int seconds);
	abstract void sendMessage();
	abstract void receiveCall();
	
	// Non Abstract Method
	void browse() {
		System.out.println("Smartphone browsing");
	}
	
	// Constructor
	public SmartPhone() {
		System.out.println("Smartphone under development");
	}

}
